<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/src/db_config.php';
$_ENV = Utils::loadConfig();

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('woocommerce-reverb-sync');
if (LOG_TO_FILE) {
  $log->pushHandler(new StreamHandler(__DIR__ . '/app.log', Logger::DEBUG));
}


function formatAndSendMail($result) {
  $body = '';
  foreach ($result as $status => $items) {
    $body .= strtoupper($status) . "\n";
    if (count($items) > 0) {
      $body .= implode(",\n", $items) . "\n\n";
    } else {
      $body .= "No items\n\n";
    }
  }
  Utils::sendMail('Sync result', $body);
}

$opts = array('http'=>array('header' => "User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\r\n")); 
$context = stream_context_create($opts);
$data = file_get_contents('https://littlefishaudio.com/wp-json/codse/v1/items', false, $context);
if ($data && is_string($data)) {
  $products = json_decode($data);
  if (count($products) > 0) {
    $log->info('fetched total items: ' . count($products));
  } else {
    $log->info('no items found.', ['data' => $data]);
    return;
  }
  $result = [
    'in-sync' => [],
    'failed' => [],
    'synced' => [],
    'exported' => [],
    'failed-export' => [],
    'failed-sync' => []
  ];
  foreach ($products as $product) {
    $wooProduct = new Product($product, Product::SOURCE_WOOCOMMERCE);
    $wooProduct->Amount = correct_amount($product);
    try {
      $status = ReverbApi::singleton()->updatePrice(
        $wooProduct,
        $log
      );
    } catch (Exception $e) {
      $status = 'failed';
    }
    $result[$status][] = $wooProduct->SKU;
  }
  if (count($products) > 0) {
    formatAndSendMail($result);
    // var_dump($result);
  }
}

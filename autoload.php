<?php

/**
 * Autoload custom classes
 */
define('LOG_TO_FILE', true);

spl_autoload_register(function ($name) {
  $folders = [
    __DIR__.'/src/api/',
    __DIR__.'/src/database/',
    __DIR__.'/src/models/'
  ];  
  foreach($folders as $folder) {
    if (file_exists($folder."$name.php")) {
      return require_once $folder."$name.php";
    }
  }
});
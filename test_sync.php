<?php

require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/autoload.php';
require_once __DIR__.'/src/db_config.php';
$_ENV = Utils::loadConfig();
  ?>
  <!doctype html>
<html>
<head>
 <meta charset="utf-8" />
 <meta name="viewport" content="width=device-width,inital-scale=1.0">
 <link href="public/app.css" rel="stylesheet" />
 <style>
     p {
         color: blue;
     }
     .success {
         color: green;
     }
     .warning {
         color: rgb(225, 205, 0);
     }
     .error {
         color: red;
     }
     pre {
         background-color: rgb(235, 235, 235);
         padding: 4px 8px;
     }
 </style>
</head>
<body>
<div class="container">
  <div style="margin: 16px auto" class="columns">
    <div class="columns six">
      <strong>Listing</strong>
      <form method="GET">
        <label>Enter the SKU of WooCommerce for Export, SKU existing in both for others.</label>
        <input type="text" name="sku" placeholder="Enter SKU" />
        <input type="submit" name="action" value="Sync *" />
        <input type="submit" name="action" value="Check deleted" />
        <p><small>* exports (if not in reverb) or syncs quantity, product status, price and title</small></p>
      </form>
     </div>
    <div class="columns six">
      <strong>Order</strong>
      <form method="GET">
        <label>Enter the order id of reverb.</label>
        <input type="text" name="order" placeholder="Enter Order Id" />
        <input type="submit" name="action" value="Export to WooCommerce" />
        <input type="submit" name="action" value="Sync Tracking" />
      </form>
    </div>
  </div>
  
  <?php
    define("__DEV__", true);
    function printJson($product) {
        if (__DEV__) {
            echo sprintf('<pre>%s</pre>', $product->toJSON(true));
        }
    }
    function printProductInfo($woocommerce, $reverb) {
      echo '<h4>Woocommerce</h4>';
      if ($woocommerce) {
          echo '<p>Name: <strong>' . $woocommerce->Name . '</strong></p>';
          echo '<p>Price: <strong>' . $woocommerce->Amount . '</strong></p>';
          printJson($woocommerce);
      } else {
          echo '<p class="warning">The given SKU doesn\'t exist in WooCommerce</p>';
      }
      echo '<h4>Reverb</h4>';
      if ($reverb) {
          echo '<p>Name: <strong>' . $reverb->Name . '</strong></p>';
          echo '<p>Price: <strong>' . $reverb->Amount . '</strong></p>';
          printJson($reverb);
      } else {
          echo '<p class="warning">The given SKU doesn\'t exist in Reverb</p>';
      }
    }
    
    function getWooProduct($sku) {
        $products = WooCommerceAPI::singleton()->getProductId($sku);
        if (count($products) === 0) {
            return null;
        }
        return new Product($products[0], Product::SOURCE_WOOCOMMERCE);
    }
    
    function getRevProduct($sku) {
      $products = ReverbApi::singleton()->getProducts(['sku' => $sku, 'state' => 'all']);
      return count($products) === 0 ? null : $products[0];
    }
    
    function printStatus($status, $msg) {
      $results = [
        'in-sync' => ['', 'Already in sync'],
        'removed' => ['success', 'The item has been marked as ended'],
        'failed' => ['error', 'Failed'],
        'synced' => ['success', 'Synced successfully'],
        'exported' => ['success', 'Exported successfully'],
        'failed-export' => ['error', 'Failed to export'],
        'failed-sync' => ['error', 'Failed to sync']
      ];
      $result = $results[$status];
      echo sprintf('<p class="%s">%s. %s</p>', $result[0], $result[1], $msg);
    }
    
    function performSync($sku) {
      $wooProduct = getWooProduct($sku);
      $reverb = getRevProduct($sku);
      printProductInfo($wooProduct, $reverb);
      $msg = '';
      try {
          if (!$wooProduct) {
              if ($reverb) {
                  $status = 'removed';
                  ReverbApi::singleton()->remove($reverb->Id);
              } else {
                  throw new Exception('The given SKU is not valid.');
              }
          } else {
              $status = ReverbApi::singleton()->updatePrice($wooProduct);
          }
      } catch (Exception $e) {
          $status = 'failed';
          $msg = $e->getMessage();
      }
      printStatus($status, $msg);
    }
    
    function performDelete($sku) {
      $woocommerce = getWooProduct($sku);
      printProductInfo($woocommerce, null);
    } 
    
    function performOrder($orderId) {
        echo '<p class="error">Not implemented in UI.</p>';
    }
    
    function performTrackingSync($orderId) {
        echo '<p class="error">Not implemented in UI.</p>';
    }
    
    function hasInput($key) {
        return isset($_GET[$key]) && strlen($_GET[$key]) > 0;
    }
?>
<div style="margin: 16px 0">
    
<?php
    if (hasInput('sku') && hasInput('action')) {
        $sku = $_GET['sku'];
        performSync($sku);
    } else if (hasInput('order') && hasInput('action')) {
        $order = $_GET['order'];
        $action = $_GET['action'];
        switch ($action) {
            case 'Export to WooCommerce':
                performOrder($order);
                break;
            case 'Sync Tracking':
                performTrackingSync($order);
                break;
        }
    }
  
  ?>
  
</div>
</div>
  </body>
 </html>
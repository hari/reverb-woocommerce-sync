<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/src/db_config.php';
$_ENV = Utils::loadConfig();

$untracked = OrderTable::getUntracked();

$ids = array_map(function ($item) {
    return $item['woo_order_id'];
}, $untracked);
if (count($ids) === 0) {
    exit(0);
}
$trackings = WooCommerceAPI::singleton()->getOrdersTracking($ids);
foreach ($untracked as $order) {
    $woo_id = $order['woo_order_id'];
    if (array_key_exists($woo_id, $trackings)) {
        $tracking = json_encode([
            'provider' => $trackings[$woo_id]->tracking_provider,
            'tracking_number' => $trackings[$woo_id]->tracking_number,
            'send_notification' => true
        ]);
        $reverb_id = json_decode($order['json'])->order_number;
        file_put_contents(__DIR__.'/shipping.txt', $tracking, FILE_APPEND);
        if (ReverbApi::singleton()->syncTracking($reverb_id, $tracking)) {
           OrderTable::setTracking($order['id'], $tracking);
          file_put_contents(__DIR__.'/shipping.txt', 'done ---> ' . $tracking, FILE_APPEND);
        }
    }
}

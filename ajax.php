<?php
require_once 'vendor/autoload.php';
require_once 'src/db_config.php';
require_once 'autoload.php';
$_ENV = Utils::loadConfig();
//handles AJAX request.
//ajax.php?action=ACTION

if (isset($_GET['action'])) {

    $action = strtolower($_GET['action']);
    unset($_GET['action']);

    $valid_actions = ['list_reverb', 'save', 'list_ecomdash', 'sync_price_info', 'add_order', 'export_as_draft', 'send_mail', 'read_orders', 'sku_diff', 'not_live'];
    if (!in_array($action, $valid_actions)) {
      echo json_encode(array('status' => 'failed', 'data' => array('Invalid Action')));
      die;
    }

    if (isset($_POST) && count($_POST) > 0) {
        echo json_encode(Utils::handleAction($action, $_POST));
    } else {
        echo json_encode(Utils::handleAction($action, $_GET));
    }
}

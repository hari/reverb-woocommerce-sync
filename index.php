<?php
session_start();
$_SESSION['user'] = 'hari';

$_ENV = []; //all the configurations will be read to this array from database
$current_view = 'create_config.php';

require_once 'vendor/autoload.php';
require_once 'autoload.php';
if (file_exists('src/db_config.php')) {
  require_once 'src/db_config.php';
  $_ENV = Utils::loadConfig();
  //8 - total must have setting
  if (count($_ENV) >= 10) {
    $current_view = 'order_sku.php';
    $pages = array('listing', 'not_live', 'queue', 'orders', 'price');
    if (isset($_GET['page']) && in_array($_GET['page'], $pages)) {
      $current_view = $_GET['page'].'.php';
    }
  }
}

if (isset($_POST) && count($_POST) > 0) {
  $current_view = 'process_request.php';
}

include 'src/views/index.php';
<?php
// this script is executed at different time with different parameters
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/autoload.php';
require_once __DIR__.'/src/db_config.php';
$_ENV = Utils::loadConfig();

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
$log = new Logger('woocommerce-reverb-sync');
if (LOG_TO_FILE) {
  $log->pushHandler(new StreamHandler(__DIR__.'/app.log', Logger::DEBUG));
}

$products = ProductTable::getNotSynced();
if (count($products) == 0) {
  $log->log('info', 'sync done');
  die('done');
}
foreach ($products as $product) {
  $others = WooCommerceAPI::singleton()->getProductId(urlencode($product->SKU));
  if (count($others) == 0) {
    $log->log('info', $product->SKU . ' does not exist in woocommerce.');
    continue;
  }
  $another = new Product($others[0], Product::SOURCE_WOOCOMMERCE);
  if ($another->Amount == $product->Amount) {
    $log->log('info', 'both amount match of ' . $another->SKU);
    continue;
  }
  $log->log('info', 'syncing amount of ' . $another->SKU . ' from ' . $product->Amount . ' to ' . $another->Amount);
  if (ReverbApi::singleton()->updatePrice(
    $another->SKU,
    $another->Amount,
    $another->Name,
    $log
  )) {
    $log->info('synced successfully');
  } else {
    $log->error('sync failed for sku: ' . $another->SKU);
  }
}
ProductTable::markSynced(
  array_map(function ($pro) {
    return $pro->SKU;
  }, $products)
);
echo 'next';

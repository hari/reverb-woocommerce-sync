const { exec } = require('child_process');

let i = 1;
function handle() {
  console.log('calling -> ' + i);
  exec('php sync.php', (err, res) => {
    if (res.includes('done')) {
      return;
    }
    i++;
    
    setTimeout(handle, 0);
  });
}

handle();

<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/src/db_config.php';
$_ENV = Utils::loadConfig();

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('woocommerce-reverb-sync');
if (LOG_TO_FILE) {
  $log->pushHandler(new StreamHandler(__DIR__ . '/app.log', Logger::DEBUG));
}

function formatAndSendMail($result) {
  $body = '';
  foreach ($result as $status => $items) {
    $body .= strtoupper($status) . "\n";
    if (count($items) > 0) {
      $body .= implode(",\n", $items) . "\n\n";
    } else {
      $body .= "No items\n\n";
    }
  }
  Utils::sendMail('Sync result', $body);
}

$data = file_get_contents('https://littlefishaudio.com/wp-json/codse/v1/updated');
if ($data && is_string($data)) {
  $products = json_decode($data);
  if ($products && count($products) > 0) {
    $log->info('fetched total items: ' . count($products));
  } else {
    $log->info('no items found.', ['data' => $data]);
    return;
  }
  $result = [
    'in-sync' => [],
    'failed' => [],
    'synced' => [],
    'exported' => [],
    'failed-export' => [],
    'failed-sync' => []
  ];
  foreach ($products as $product) {
    $wooProduct = new Product($product, Product::SOURCE_WOOCOMMERCE);
    $wooProduct->Amount = correct_amount($product);
    try {
      $status = ReverbApi::singleton()->updatePrice(
        $wooProduct,
        $log
      );
    } catch (Exception $e) {
      $status = 'failed';
    }
    $result[$status][] = $wooProduct->SKU;
  }
  if (count($products) > 0) {
    formatAndSendMail($result);
    // var_dump($result);
  }
}

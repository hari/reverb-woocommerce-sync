<?php

/**
 * Stores orders from reverb.com
 * These orders are processed by cron job, then mark as sent
 */
class ProductTable
{
  const STATE_NONE = 0x0;
    // sent as mail
  const STATE_SENT = 0x1;
    // price information synced
  const STATE_SYNCED = 0x2;
    // exported as draft
  const STATE_EXPORTED = 0x3;

  public static function create()
  {
    $sql = "CREATE TABLE IF NOT EXISTS `" . PRODUCT_TABLE_NAME . "` ( `id` INT NOT NULL AUTO_INCREMENT , `product_date` DATETIME NOT NULL, `sku` VARCHAR(200) NOT NULL , `json` LONGTEXT NOT NULL , `added_on` DATETIME NOT NULL ,`state` VARCHAR(255) NOT NULL, `price` float DEFAULT 0, `source` TINYINT NOT NULL , `synced` BOOLEAN NOT NULL , PRIMARY KEY (`id`));";
    return Connection::execute($sql);
  }

  public static function put($product)
  {
    $source = $product->getSource();
    $existing = self::get($product->SKU, $source);
    if ($existing == null) {
            //new item, need to store it
      return self::insert($product);
    } elseif ($existing['json'] == $product->raw_json) {
            //no need to run query
      return true;
    }
        //old value need to update
    return self::update($product);
  }

  private static function update($product)
  {
    $source = $product->getSource();
    $sql = sprintf('UPDATE `%s` SET `price` = :amount, `json`= :json, `state` = :state, `added_on` = :updated_date WHERE `sku` = :sku AND `source` = :source', PRODUCT_TABLE_NAME);
    $now = date('Y-m-d H:m:s');
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':sku', $product->SKU, PDO::PARAM_STR);
    $stm->bindParam(':json', $product->raw_json, PDO::PARAM_STR);
    $stm->bindParam(':state', $product->Status, PDO::PARAM_STR);
    $stm->bindParam(':amount', $product->Amount, PDO::PARAM_STR);
    $stm->bindParam(':updated_date', $now, PDO::PARAM_STR);
    $stm->bindParam(':source', $source, PDO::PARAM_INT);
    return $stm->execute();
  }

  private static function insert($product)
  {
    $source = $product->getSource();
    $sql = sprintf('INSERT INTO `%s` (`price`, `product_date`, `sku`, `json`, `state`, `source`, `added_on`, `synced`) 
        VALUES (:amount, :pd, :sku, :json, :state, :source, :ao, 0)', PRODUCT_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':sku', $product->SKU, PDO::PARAM_STR);
    $stm->bindParam(':json', $product->raw_json, PDO::PARAM_STR);
    $stm->bindParam(':state', $product->Status, PDO::PARAM_STR);
    $stm->bindParam(':ao', $now, PDO::PARAM_STR);
    $stm->bindParam(':source', $source, PDO::PARAM_INT);
    $stm->bindParam(':amount', $product->Amount, PDO::PARAM_STR);
    $stm->bindParam(':pd', $product->PublishDate, PDO::PARAM_STR);
    $now = date('Y-m-d H:m:s');
    $stm->bindParam(':ao', $now, PDO::PARAM_STR);
    return $stm->execute();
  }

  public static function get($sku, $source)
  {
    $sql = sprintf('SELECT * FROM `%s` WHERE `sku` = :name AND `source` = :source', PRODUCT_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':name', $sku, PDO::PARAM_STR);
    $stm->bindParam(':source', $source, PDO::PARAM_INT);
    if ($stm->execute()) {
      return $stm->fetch();
    }
    return null;
  }

  public static function getNotLiveSKUs()
  {
    return array_map(function ($item) {
      return $item['sku'];
    }, self::getNotLive());
  }

  public static function getNotLive()
  {
    $skus = sprintf("SELECT sku FROM `%s` WHERE state not in ('draft', 'live') AND source = %d AND synced = 0", PRODUCT_TABLE_NAME, Product::SOURCE_REVERB);
    $sql = sprintf("SELECT * FROM `%s` WHERE sku in (%s) AND source = %d", PRODUCT_TABLE_NAME, $skus, Product::SOURCE_WOOCOMMERCE);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return $stm->fetchAll();
    }
    return array();
  }

  public static function getDiff($to_source = Product::SOURCE_REVERB, $from_source = Product::SOURCE_WOOCOMMERCE)
  {
    $all = self::getDiffAll($to_source, $from_source);
    return array_map(function ($item) {
      return $item['sku'];
    }, $all);
  }

  public static function getDiffAll($to_source = Product::SOURCE_REVERB, $from_source = Product::SOURCE_WOOCOMMERCE)
  {
    $skus = sprintf("SELECT sku FROM `%s` WHERE source = %d AND synced = %d", PRODUCT_TABLE_NAME, $from_source, self::STATE_NONE);
    $sql = sprintf("SELECT * FROM `%s` WHERE sku not in (%s) AND source = %d AND synced = %d", PRODUCT_TABLE_NAME, $skus, $to_source, self::STATE_NONE);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return $stm->fetchAll();
    }
    return array();
  }

  public static function getFrom($source)
  {
    $sql = sprintf("SELECT * FROM `%s` WHERE source = %d", PRODUCT_TABLE_NAME, $source);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return $stm->fetchAll();
    }
    return array();
  }

  public static function getCountOf($source)
  {
    $sql = sprintf("SELECT sku FROM `%s` WHERE source = %d", PRODUCT_TABLE_NAME, intval($source));
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return count($stm->fetchAll());
    }
    return 0;
  }

  /**
   * Marks the products as mailed after being mailed to the admin
   *
   * @param array $skus
   * @return boolean
   */
  public static function markSent($skus)
  {
    return self::mark($skus, self::STATE_SENT);
  }

  /**
   * Marks the products as synced i.e the price information
   *
   * @param array $skus
   * @return boolean
   */
  public static function markSynced($skus)
  {
    return self::mark($skus, self::STATE_SYNCED);
  }

  /**
   * Marks the products as exported i.e the from woocommerce to reverb
   *
   * @param array $skus
   * @return boolean
   */
  public static function markExported($skus)
  {
    return self::mark($skus, self::STATE_EXPORTED);
  }

  /**
   * Does the required update to the synced status
   *
   * @param array $skus
   * @param int $state
   * @return boolean
   */
  private static function mark($skus, $state)
  {
    $str_sku = implode(",", array_map(
      function ($sku) {
        return "'$sku'";
      },
      $skus
    ));
    $sql = sprintf("UPDATE %s SET synced = %d WHERE sku IN (%s)", PRODUCT_TABLE_NAME, $state, $str_sku);
    $stm = Connection::get()->prepare($sql);
    return $stm->execute();
  }

  public static function getSameSKUs()
  {
    $ecos = sprintf(
      "SELECT sku FROM %s WHERE source = %d AND synced = %d",
      PRODUCT_TABLE_NAME,
      Product::SOURCE_WOOCOMMERCE,
      self::STATE_NONE
    );
    $sql = sprintf(
      "SELECT sku FROM %s WHERE sku IN (%s) AND source = %d AND synced = %d",
      PRODUCT_TABLE_NAME,
      $ecos,
      Product::SOURCE_REVERB,
      self::STATE_NONE
    );
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return array_map(function ($item) {
        return $item['sku'];
      }, $stm->fetchAll());
    }
    return array();
  }

  public static function getDiffPrice($make_request = false)
  {
    $same = self::getSameSKUs();
    $limit = min(5, count($same));
    $items = [];
    for ($i = 0; $i < $limit; $i++) {
      $raw_eco = new Product(
        self::get($same[$i], Product::SOURCE_WOOCOMMERCE)['json'],
        Product::SOURCE_WOOCOMMERCE
      );
      $raw_rev = new Product(
        self::get($same[$i], Product::SOURCE_REVERB)['json'],
        Product::SOURCE_REVERB,
        $make_request
      );
      if ($raw_eco->Amount == $raw_rev->Amount) {
        continue;
      }
      $items[$same[$i]] = array($raw_eco->Amount, $raw_rev->Amount);
    }
    return $items;
  }

  /**
   * It will fetch price information of next 10 products from WooCommerce
   * and then update the price as well as WOOCOMMERCE_PRICE_POINTER to current id
   *
   * @return void
   */
  private static function getNextForPrice(&$id)
  {
    $sql = sprintf(
      "SELECT id, json FROM %s WHERE source = '%s' AND id >= %d LIMIT 10",
      PRODUCT_TABLE_NAME,
      Product::SOURCE_WOOCOMMERCE,
      $_ENV['WOOCOMMERCE_PRICE_POINTER']
    );
    $stmt = Connection::get()->prepare($sql);
    if ($stmt->execute()) {
      $results = $stmt->fetchAll();
      if (count($results) > 0) {
        $id = end($results)['id'];
      } else {
        $id = 0;
      }
      return array_map(function ($item) {
        return new Product($item['json'], Product::SOURCE_WOOCOMMERCE);
      }, $results);
    }
    return array();
  }

  public static function updatePrice()
  {
    $list = self::getNextForPrice($id);
    foreach ($list as $product) {
      self::update($product);
    }
    SettingTable::put('WOOCOMMERCE_PRICE_POINTER', $id + 1);
  }

  public static function getNotSynced() {
    $sql = sprintf(
      "SELECT id, json FROM %s WHERE source = '%s' AND synced <> %d LIMIT 20",
      PRODUCT_TABLE_NAME,
      Product::SOURCE_REVERB,
      self::STATE_SYNCED
    );
    $stmt = Connection::get()->prepare($sql);
    if ($stmt->execute()) {
      $results = $stmt->fetchAll();
      return array_map(function ($item) {
        return new Product($item['json'], Product::SOURCE_REVERB);
      }, $results);
    }
    return [];
  }

}

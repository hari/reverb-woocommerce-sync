<?php

/**
 * Stores orders from reverb.com
 * These orders are processed by cron job, then mark as sent
 */
class OrderTable
{

  public static function create()
  {
    $sql = "CREATE TABLE IF NOT EXISTS `" . ORDER_TABLE_NAME . "` ( `id` INT NOT NULL AUTO_INCREMENT , `sku` VARCHAR(200) NOT NULL , `rev_order_id` INT NOT NULL DEFAULT -1, `json` LONGTEXT NOT NULL , `added_on` DATETIME NOT NULL , `sent` BOOLEAN NOT NULL, `woo_order_id` INT, `tracking_json` LONGTEXT DEFAULT NULL , PRIMARY KEY (`id`));";
    return Connection::execute($sql);
  }
  
  public static function getByOrderId($id)
  {
    $sql = sprintf('SELECT * FROM `%s` WHERE `rev_order_id` = :id', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':id', $id, PDO::PARAM_INT);
    if ($stm->execute()) {
      return $stm->fetch();
    }
    return null;   
  }

  public static function put($sku, $json)
  {
    $existing = self::getByOrderId(json_decode($json)->order_number);
    if ($existing == null || $existing['json'] != $json) {
            //there can be multiple order with same SKU
      return self::insert($sku, $json);
    }
    return true;
  }

  private static function update($sku, $json)
  {
    $sql = sprintf('UPDATE `%s` SET `json`= :json WHERE `sku` = :sku', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':json', $json, PDO::PARAM_STR);
    $stm->bindParam(':sku', $sku, PDO::PARAM_STR);
    return $stm->execute();
  }

  private static function insert($sku, $json)
  {
      $id = json_decode($json)->order_number;
    $sql = sprintf('INSERT INTO `%s` (`sku`,`json`, `rev_order_id`, `added_on`, `sent`) VALUES (:sku, :json, :rev, :da, 0)', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':sku', $sku, PDO::PARAM_STR);
    $stm->bindParam(':rev', $id, PDO::PARAM_STR);
    $stm->bindParam(':json', $json, PDO::PARAM_STR);
    $now = date('Y-m-d H:m:s');
    $stm->bindParam(':da', $now, PDO::PARAM_STR);
    return $stm->execute();
  }
  
  public static function setOrderId($local_id, $woo_order_id) {
      file_put_contents(__DIR__.'/setting.txt', $local_id . '->' . $woo_order_id);
    $sql = sprintf('UPDATE `%s` SET `woo_order_id`= :woo_order_id WHERE `id` = :id', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':woo_order_id', $woo_order_id, PDO::PARAM_INT);
    $stm->bindParam(':id', $local_id, PDO::PARAM_INT);
    return $stm->execute();
  }
  
  public static function setTracking($local_id, $tracking) {
    $sql = sprintf('UPDATE `%s` SET `tracking_json`= :tracking WHERE `id` = :id', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':tracking', $tracking, PDO::PARAM_STR);
    $stm->bindParam(':id', $local_id, PDO::PARAM_INT);
    return $stm->execute();
  }
  
  public static function getUntracked() {
    $sql = sprintf('SELECT * FROM `%s` WHERE tracking_json IS NULL AND woo_order_id <> -1', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return $stm->fetchAll();
    }
    return array();
  }

  public static function get($sku)
  {
    $sql = sprintf('SELECT * FROM `%s` WHERE `sku` = :sku', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':sku', $sku, PDO::PARAM_STR);
    if ($stm->execute()) {
      return $stm->fetch();
    }
    return null;
  }

  public static function getById($id)
  {
    $sql = sprintf('SELECT * FROM `%s` WHERE `id` = :id', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':id', $id, PDO::PARAM_STR);
    if ($stm->execute()) {
      return $stm->fetch();
    }
    return null;
  }

  public static function markDone($sku)
  {
    $sql = sprintf('UPDATE `%s` SET `sent`= 1 WHERE `sku` = :sku', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':sku', $sku, PDO::PARAM_STR);
    return $stm->execute();
  }

  public static function all()
  {
    $sql = sprintf('SELECT * FROM `%s` order by sent asc', ORDER_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return $stm->fetchAll();
    }
    return array();
  }
}

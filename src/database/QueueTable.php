<?php

/**
 * Products and orders are passed here to perform actions.
 */
class QueueTable
{

  public static function create()
  {
    $sql = "CREATE TABLE IF NOT EXISTS `" . QUEUE_TABLE_NAME . "` ( `id` INT NOT NULL AUTO_INCREMENT , `action` VARCHAR(200) NOT NULL, `sku` VARCHAR(200) NOT NULL, `added_on` DATETIME NOT NULL, `executed_on` DATETIME, `message` TEXT, PRIMARY KEY (`id`));";
    return Connection::execute($sql);
  }

  public static function markDone($id, $message = 'done')
  {
    $sql = sprintf('UPDATE `%s` SET `executed_on` = :eo, `message` = :message WHERE `id` = :id', QUEUE_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $now = date('Y-m-d H:m:s');
    $stm->bindParam(':eo', $now, PDO::PARAM_STR);
    $stm->bindParam(':message', $message, PDO::PARAM_STR);
    $stm->bindParam(':id', $id, PDO::PARAM_STR);
    return $stm->execute();
  }

  public static function insert($sku, $action)
  {
    if (self::get($sku, $action)) {
      return true;
    }
    $sql = sprintf('INSERT INTO `%s` (`sku`, `action`, `added_on`) VALUES (:sku, :action, :da)', QUEUE_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':sku', $sku, PDO::PARAM_STR);
    $stm->bindParam(':action', $action, PDO::PARAM_STR);
    $now = date('Y-m-d H:m:s');
    $stm->bindParam(':da', $now, PDO::PARAM_STR);
    return $stm->execute();
  }
  
  public static function getNext()
  {
    $sql = sprintf("SELECT * FROM %s WHERE executed_on IS NULL AND message IS NULL ORDER BY id LIMIT 2", QUEUE_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      $rows = $stm->fetchAll();
      if (empty($rows)) {
          return self::getFailed();
      }
      return $rows;
    }
    return array();
  }

  public static function getById($id)
  {
    $sql = sprintf('SELECT * FROM `%s` WHERE `id` = :id', QUEUE_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':id', $id, PDO::PARAM_STR);
    if ($stm->execute()) {
      return $stm->fetch();
    }
    return array();
  }

  public static function get($sku, $action)
  {
    $sql = sprintf('SELECT * FROM `%s` WHERE `sku` = :sku AND `action` = :action', QUEUE_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    $stm->bindParam(':sku', $sku, PDO::PARAM_STR);
    $stm->bindParam(':action', $action, PDO::PARAM_STR);
    if ($stm->execute()) {
      return $stm->fetch();
    }
    return null;
  }

  public static function queued()
  {
    $sql = sprintf("SELECT * FROM `%s` WHERE executed_on IS NULL AND message IS NULL", QUEUE_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return $stm->fetchAll();
    }
    return array();
  }

  public static function getFailed() {
    $sql = sprintf("SELECT * FROM %s WHERE message = 'failed'", QUEUE_TABLE_NAME);
    $stm = Connection::get()->prepare($sql);
    if ($stm->execute()) {
      return $stm->fetchAll();
    }
    return array();
  }

}

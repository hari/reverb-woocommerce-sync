<?php
define('DB_HOST', '127.0.0.1');
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_NAME', 'reverb');
define('TABLE_NAME', 'res_setting');
define('ORDER_TABLE_NAME', 'res_order');
define('PRODUCT_TABLE_NAME', 'res_product');
define('QUEUE_TABLE_NAME', 'res_queue');
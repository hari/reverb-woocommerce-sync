<?php
  if (isset($_GET['execute'])) {
    $task = QueueTable::getById($_GET['execute']);
    if ($task != null && Utils::execute($task)) {
      echo '<p>Task id ['.$task['id'].'] has been executed.</p>';
    } else {
      echo '<p>Failed to execute.</p>';
    }
  }
  $queue = QueueTable::queued();
?>
<div class="row">
<div class="columns eight">
<h1>Queued items [<?php echo count($queue); ?>]</h1>
<ol class="box">
<?php
  if (count($queue) > 0) :
    foreach ($queue as $item) :
 ?>
   <li class="box-row">
    <h6><strong><?php echo $item['sku']; ?></strong> queued on <?php echo $item['added_on'] ;?></h6>
    <p class="row">Action: <strong><?php echo $item['action']; ?></strong> &nbsp; <a class="u-pull-right button-small" href="?page=queue&execute=<?php echo $item['id']; ?>">Execute Now</a></p>
   </li>
 <?php 
   endforeach;
  else:
   echo '<li class="box-row">No items.</li>';
  endif;
?>

</ol>
</div>
<div class="columns four">
<h1>Failed items</h1>
<ol class="box">
<?php
  $queue = QueueTable::getFailed();
  if (count($queue) > 0) :
    foreach ($queue as $item) :
 ?>
   <li class="box-row">
    <h6><strong><?php echo $item['sku']; ?></strong> queued on <?php echo $item['added_on'] ;?></h6>
    <p class="row">Action: <strong><?php echo $item['action']; ?></strong></p>
   </li>
 <?php 
   endforeach;
  else:
   echo '<li class="box-row">No items.</li>';
  endif;
?>
</div>
</div>
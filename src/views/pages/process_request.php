<?php

//Saves the db_config or other configuration from POST request

$empty_fields = Utils::findEmptyKey($_POST);
if (empty($empty_fields)) {
    if (isset($_POST['DB_HOST'])) {
        Utils::createConfig();
        try {
            require_once 'src/db_config.php';
            if (SettingTable::create()) {
                OrderTable::create();
                ProductTable::create();
                QueueTable::create();
                echo '<h3 class="green">Success</h3>';
                echo '<p style="margin-bottom:8px">The database settings have been saved.</p>';
                echo '<a href="/">Home</a>';
            } else {
                error();
            }
        } catch (Exception $e) {
            error();
        }
    } else {
        Utils::saveSetting();
        // these page numbers are used to pull data from specific API.
        SettingTable::put('WOOCOMMERCE_PAGE_NO', 1);
        SettingTable::put('REVERB_PAGE_NO', 1);
        // this will point to the current row_id in table
        // upto which the price has been synced
        SettingTable::put('WOOCOMMERCE_PRICE_POINTER', 1);
        $_ENV = Utils::loadConfig();
        Utils::saveConditions();
        echo '<h3 class="green">Success</h3>';
        echo '<p style="margin-bottom:8px">The settings have been saved.</p>';
        echo '<a href="/">Home</a>';
    }
} else {
    echo '<h3 class="red">Error</h3>';
    echo '<p style="margin-bottom:8px">Opps! you forgot to fill <strong>'.implode(',', $empty_fields).'</strong></p>';
    echo '<a href="/">Try Again</a>';
}

function error()
{
    unlink('src/db_config');
    echo '<h3 class="red">Error</h3>';
    echo '<p>The database settings didn\'t work.</p>';
    echo '<ol><li>Please make sure username and password is correct.</li><li>Please make sure database has been created.</li></ol>';
    echo '<a href="/">Try Again</a>';
}

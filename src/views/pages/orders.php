<?php
 if (isset($_GET['from'])) {
   $params = array('page' => 1, 'from' => $_GET['from'], 'to' => date('Y-m-d H:i:s'));
   $count = count(Utils::handleAction('read_orders', $params)['data']);
   echo $count . ' orders found since ' . $_GET['from'];
 }
?>
<div class="row">
  <?php
  $orders = OrderTable::all(); ?>
  <h1>Orders [<?php echo count($orders); ?>]</h1>
  <form action="" class="row">
   <div class="columns one">
    <h5>Since</h5>
   </div>
   <input type="hidden" name="page" value="orders" />
   <input class="columns eight" type="text" name="from" value="<?php echo date('Y-m-d'); ?>" />
   <button type="submit" class="columns three button-small">Fetch Orders</button>
  </form><br />
  <table id="dataTable" class="box">
  <thead>
    <tr class="box-row">
      <th>Item</th>
      <th>Transferred?</th>
    </tr>
  </thead>
  <?php
  foreach ($orders as $pro) :
   $order = new Order($pro['json'], md5(rand(1111, 9999).time()));
   ?>
   <tr class="box-row">
   <td><?php echo $order->sku; ?></td>
   <td><?php echo $pro['sent'] == 1 ? 'Yes' : 'No'; ?></td>
   </tr>
  <?php endforeach; ?>
  </table>
 </div>
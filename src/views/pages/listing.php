<div class="row">
 <div class="columns six">
  <?php
  $products = ProductTable::getFrom(Product::SOURCE_REVERB); ?>
  <h1>Reverb [<?php echo count($products); ?>]</h1>
  <ol class="box">
  <?php
  if (count($products) == 0) {
    echo '<li class="box-row">No SKUs</li>';
  } else {
  foreach ($products as $pro) :
    $product = new Product(json_decode($pro['json']), $pro['source'], false);
   ?>
   <li class="box-row">
      <h5><?php echo $product->Name; ?></h5>
      <div class="row">
        <p class="u-pull-left">SKU: <strong><?php echo $product->SKU; ?></strong></p>
        <p class="u-pull-right">From: <strong><?php echo $product->Source; ?></strong></p>
      </div>
   </li>
  <?php
   endforeach;
     } ?>
  </ol>
 </div>
 <div class="columns six">
  <?php
  $products = ProductTable::getFrom(Product::SOURCE_WOOCOMMERCE);
  ?>
  <h1>WooCommerce [<?php echo count($products); ?>]</h1>
  <ol class="box">
  <?php
  if (count($products) == 0) {
    echo '<li class="box-row">No SKUs</li>';
  } else {
  foreach ($products as $pro) :
    $product = new Product(json_decode($pro['json']), $pro['source'], false);
   ?>
   <li class="box-row">
      <h5><?php echo $product->Name; ?></h5>
      <div class="row">
        <p class="u-pull-left">SKU: <strong><?php echo $product->SKU; ?></strong></p>
        <p class="u-pull-right">From: <strong><?php echo $product->Source; ?></strong></p>
      </div>
   </li>
  <?php
   endforeach;
     } ?>
  </ol>
 </div>
</div>
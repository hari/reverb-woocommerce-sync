<?php
/**
 *  This page will dynamically ask for the required config that are missing
 */

$db_config = [
  'title_db' => 'Database Configuration',
  'DB_HOST' => 'localhost',
  'DB_USER' => 'root',
  'DB_PASS' => 'password',
  'DB_NAME' => 'db_reverb_eco'
];
$other_config = [
  'title_rev' => 'Reverb.com\'s details',
  'REVERB_TOKEN' => '2e4f74e2dbf7c63cef3ca4ecf4792f8aa8761d5fbc25fc3fede74be6735597b9',
  'REVERB_API_URL' => 'https://api.reverb.com/api',
  'REVERB_START_DATE' => 'The start date to check for listing and orders.',
  'SHIPPING_PROFILE_ID' => 'Shipping profile id to add products to reverb.',
  'title_eco' => 'Woocommerce\'s details',
  'WOOCOMMERCE_CONSUMER_KEY' => 'ck_bce1d36e7ab791757820f04571f55322cfed8e91',
  'WOOCOMMERCE_CONSUMER_SECRET' => 'cs_7bdfe00ab560751ac56e19214e3c5a7b3ae681fd',
  'WOOCOMMERCE_STORE_URL' => 'https://littlefishaudio.com',
  'WOOCOMMERCE_START_DATE' => 'The start date to check for listing.',
  'title_personal' => 'Notification Setting',
  'NAME' => 'Full Name',
  'EMAIL' => 'Email Address'
];


$needed_config = file_exists('src/db_config.php') ? $other_config : $db_config;

echo Utils::generateForm($_ENV, $needed_config);

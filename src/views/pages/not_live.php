<?php
  $skus = ProductTable::getNotLive();
  ?>
<div class="row">
  <h1>Not Live SKUs [<?php echo count($skus); ?>]</h1>
  <table id="dataTable" class="box">
   <thead>
    <tr>
     <th>Name</th>
     <th>SKU</th>
     <th>Make Live</th>
    </tr>
   </thead>
  <?php 
    foreach ($skus as $sku) :
    $product = new Product($sku['json'], Product::SOURCE_WOOCOMMERCE, false);
  ?>
   <tr class="box-row">
    <td><?php echo $product->Name; ?></td>
    <td><?php echo $product->SKU; ?></td>
    <td><a href="?page=not_live&amp;live=<?php echo $sku['id']; ?>" class="button-small">GO</a></td>
   </tr>
  <?php endforeach; ?>
  </table>
 </div>
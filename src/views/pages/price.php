<?php
 if (isset($_GET['sync'])) {
   $products = ProductTable::getDiffPrice();
   $inserted = array();
   foreach($products as $sku => $i) {
    if (QueueTable::insert($sku, 'sync_price')) {
      $inserted[] = $sku;
    }
   }
   ProductTable::markSynced($inserted);
 }
?>
<div class="row">
  <?php $products = ProductTable::getDiffPrice(true); ?>
  <h1>Price Info [<?php echo count($products); ?>]</h1>
  <a class="button-small" href="?page=price&amp;sync=1">Sync Now</a></p>
  <br />
  <table id="dataTable" class="box">
  <thead>
    <tr class="box-row">
    <td>SKU</td>
    <td>Reverb Price</td>
    <td>WooCommerce Price</td>
    <td>Synced?</td>
    </tr>
   </thead>
   <?php foreach ($products as $sku => $det) : ?>
   <tr class="box-row">
    <td><strong><?php echo $sku; ?></strong></td>
    <td><strong><?php echo $det[1]; ?></strong></td>
    <td><strong><?php echo $det[0]; ?></strong></td>
    <td>Yes</td>
  </tr>
  <?php endforeach; ?>
  </table>
 </div>
<div>
 <h1> &nbsp; </h1>
 <p><strong>Note</strong>: The price will be synced if the amount from WooCommerce greater than that of reverb.</p>
 <p>This page shows only 5 items at a time.</p>
</div>
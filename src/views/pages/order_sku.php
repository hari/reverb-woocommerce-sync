<?php
if (!(isset($_SESSION) && isset($_SESSION['user']))) {
    die('Authorization failed.');
}
$rev_from_date = $_ENV['REVERB_START_DATE'];
$eco_from_date = $_ENV['WOOCOMMERCE_START_DATE'];
$rev_skus = ProductTable::getDiffAll();
$eco_skus = ProductTable::getDiffAll(Product::SOURCE_WOOCOMMERCE, Product::SOURCE_REVERB);
?>
<div class="row">
<section class="columns six">
  <h4>SKU NOT IN WOOCOMMERCE</h4>
  <div class="row info">
    Total: <strong id="rev_count"><?php echo count($rev_skus); ?></strong>
    <a href="javascript://" class="button-small u-pull-right">SEND MAIL</a>
  </div>
  <table class="box" id="rev_skus">
    <thead>
     <tr>
       <th>PRODUCT NAME</th>
       <th>SKU</th>
       <th>STATUS</th>
     </tr>
    </thead>
    <?php
      foreach($rev_skus as $sku) :
        $product = new Product($sku['json'], Product::SOURCE_REVERB, false); 
    ?>
    <tr class="box-row">
     <td><?php echo $product->Name; ?></td>
     <td><?php echo $product->SKU; ?></td>
     <td><?php echo $product->Status; ?></td>
    </tr>
    <?php endforeach; ?>
  </table>
</section>
<section class="columns six">
  <h4>SKU NOT IN REVERB</h4>
  <div class="info row">
   Total: <strong id="eco_count"><?php echo count($eco_skus); ?></strong>
   <a href="javascript://" onclick="sendMail();" disabled class="button-small u-pull-right">Send Mail</a>
  </div>
  <table class="box" id="eco_skus">
    <thead>
     <tr>
       <th>SKU</th>
       <th>PRODUCT NAME</th>
     </tr>
    </thead>
    <?php
      foreach($eco_skus as $sku) :
        $product = new Product($sku['json'], Product::SOURCE_WOOCOMMERCE, false); 
    ?>
    <tr class="box-row">
     <td><?php echo $product->SKU; ?></td>
     <td><?php echo $product->Name; ?></td>
    </tr>
    <?php endforeach; ?>
  </table>
</section>
</div>
<script type="text/javascript" src="public/app.js"></script>
<script type="text/javascript">
 function sendMail() {
   var target = document.getElementById('skus');
   if (target.children.length == 0) {
     return alert('Nothing to mail');
   }
   var body = '';
   for(var c in target.children) {
     var item = target.children[c];
     if (!(item instanceof HTMLLIElement)) continue;
     var s = item.getElementsByTagName('strong');
     for(var p in s) {
     if (!(s[p] instanceof HTMLElement)) continue;
         body += s[p].textContent + ',';
     }
     body = body.substring(0, body.length - 1) + '\r\n';
   }
   if (body.length == 0) {
     alert('Nothing to mail');
     return;
   }
   promise
   .post('ajax.php?action=send_mail',{
     'subject': 'SKUs',
     'body': body
   })
   .then(function (err, res, xhr) {
     if (err) {
       return alert('Error occured while sending request.');
     }
     res = JSON.parse(res);
     if (res.status == 'failed') {
       alert(res.data[0]);
     } else {
       clearNode(target);
       document.getElementById('sku_count').textContent = 0;
       alert('Mail sent!');
     }
   })
 }
 document.onreadystatechange = function () {
   if (document.readyState == 'complete') {
    $('#rev_skus').DataTable();
    $('#eco_skus').DataTable();
   }
 }
</script>

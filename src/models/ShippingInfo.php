<?php

//   'shipping' => [
//       'first_name' => 'John',
//       'last_name' => 'Doe',
//       'address_1' => '969 Market',
//       'address_2' => '',
//       'city' => 'San Francisco',
//       'state' => 'CA',
//       'postcode' => '94103',
//       'country' => 'US'
//   ],

class ShippingInfo
{
  public $first_name;
  public $last_name;
  public $address_1;
  public $address_2;
  public $city;
  public $state;
  public $postcode;
  public $country;
  public $phone;

  public function __construct($raw_order)
  {
    $this->first_name = $raw_order->buyer_first_name;
    $this->last_name = $raw_order->buyer_last_name;
    if (property_exists($raw_order, 'shipping_address')) {
      $this->postcode = $raw_order->shipping_address->postal_code;
      $this->state = $raw_order->shipping_address->region;
      $this->country = $raw_order->shipping_address->country_code;
      $this->city = $raw_order->shipping_address->display_location;
      $this->address_1 = $raw_order->shipping_address->street_address;
      $this->address_2 = $raw_order->shipping_address->extended_address;
      $this->phone = $raw_order->shipping_address->phone;
    }
  }
}

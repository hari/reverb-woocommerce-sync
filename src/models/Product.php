<?php
// has propery
function hp($object, $prop)
{
    return property_exists($object, $prop);
}
/**
 * A minimal product representation as required in this project.
 */
class Product
{
  const SOURCE_WOOCOMMERCE = 0x0;
  const SOURCE_REVERB = 0x1;

  public $SKU;
  public $Id;
  public $Name;
  public $Source = 'reverb.com';
  public $Amount = 0;
  public $Currency = "USD";
  public $PublishDate = null;
  /**
   * @var string Stores the Status information like Active, Inactive, Draft etc
   */
  public $Status = "publish";
  public $raw_json;
  public $ProductInfo; // the raw information
  public $IsStock = false;
  public $Quantity = 0;
  public $HasStock = false;

  public function __construct($raw_data, $source, $load_info = true)
  {
    if (is_string($raw_data)) {
      $this->raw_json = $raw_data;
      $raw_data = json_decode($raw_data);
    } else {
      $this->raw_json = json_encode($raw_data);
    }
    if ($source == Product::SOURCE_WOOCOMMERCE) {
        if (!empty($raw_data)) {
            $this->Id = $raw_data->id;
            $this->Amount = $raw_data->price;
            if (hp($raw_data, 'sku')) {
                $this->SKU = $raw_data->sku;
            }
            $this->IsStock = hp($raw_data, 'manage_stock') ? $raw_data->manage_stock : false;
            $this->Quantity = hp($raw_data, 'stock_quantity') ? intval($raw_data->stock_quantity) : 0;
            if (hp($raw_data, 'name')) {
                $this->Name = $raw_data->name;
            }
            if (hp($raw_data, 'status')) {
                $this->Status = $raw_data->status;
            }
            if (hp($raw_data, 'sale_price') && is_numeric($raw_data->sale_price)) {
                $this->Amount = min($raw_data->price, $raw_data->sale_price);
            }
            if (hp($raw_data, 'stock_status')) {
                $this->HasStock = $raw_data->stock_status == 'instock';
            }
        }
        $this->Currency = "USD";
        $this->PublishDate = date('Y-m-d');
        $this->Source = 'littlefishaudio.com';
    } else if (!empty($raw_data)) {
		if (hp($raw_data, 'sku')) {
			$this->SKU = $raw_data->sku;
		}
		if (hp($raw_data, 'title')) {
			$this->Name = $raw_data->title;
		}
		if (hp($raw_data, 'state')) {
			$this->Status = $raw_data->state->description;
		}
		if (hp($raw_data, 'price')) {
			$this->Amount = $raw_data->price->amount;
			$this->Currency = $raw_data->price->currency;
		}
		if (hp($raw_data, 'published_at')) {
			$this->PublishDate = substr($raw_data->published_at, 0, 10);
		}
		if (hp($raw_data, 'has_inventory')) {
			$this->IsStock = $raw_data->has_inventory;
		}
		if (hp($raw_data, 'inventory')) {
			$this->Quantity = $raw_data->inventory;
		}
		$this->Id = $raw_data->id;
    }
    if ($load_info) {
        $this->setProductInfo($raw_data);
    }
}

  public function isActive()
  {
      if ($this->isFrom(self::SOURCE_REVERB)) {
          return $this->Status == 'Live';
      }
      if ($this->IsStock) {
          return $this->Quantity > 0 && $this->Status == 'publish';
      }
      return $this->Status == 'publish';
  }

  public function __toString()
  {
    return $this->SKU;
  }

  public function isFrom($source)
  {
    $current_source = self::SOURCE_WOOCOMMERCE;
    if ($this->Source === 'reverb.com') {
      $current_source = self::SOURCE_REVERB;
    }
    return $source === $current_source;
  }

  public function getSource()
  {
    return (($this->Source == 'reverb.com') ? self::SOURCE_REVERB : self::SOURCE_WOOCOMMERCE);
  }

  public function setProductInfo($info)
  {
    $this->ProductInfo = $info;
  }

  /**
   * Creates a JSON compatible with reverb.com's API for creating Draft.
   *
   * @return string JSON representation of the product
   */
  public function toJSON($forUpdate = false)
  {
    $product = new stdClass;
    $product->title = $this->Name;
    $product->sku = $this->SKU;
    $product->publish = $this->Status === 'publish' || $this->Status === 'Live';
    $product->upc_does_not_apply = true;
    if ($this->IsStock && $this->Quantity > 0) {
        $product->has_inventory = true;
        if ($this->isFrom(self::SOURCE_REVERB)) {
            $product->inventory = $this->Quantity;
        } else if (hp($product, 'has_inventory') && $product->has_inventory && $product->publish) {
            $product->inventory = $this->Quantity;
        } else {
            unset($product->has_inventory);
        }
    }
    if (!$forUpdate) {
      $product->shipping_profile_id = $_ENV['SHIPPING_PROFILE_ID'];
      $category = new stdClass;
      // FIXIT: Need to change this category
      $category->uuid = '62835d2e-ac92-41fc-9b8d-4aba8c1c25d5';
      $product->categories = array($category);
      $conditon = new stdClass;
      $conditon->uuid = '7c3f45de-2ae0-4c81-8400-fdb6b1d74890'; // brand new
      // not availble in wooCommerce. needs Review
      // if (property_exists($this->ProductInfo, 'GlobalListingAttributes')) {
      //   $guessed_condition = SettingTable::get($this->ProductInfo->GlobalListingAttributes->Condition);
      //   if ($guessed_condition && count($guessed_condition) > 0) {
      //     $conditon->uuid = $guessed_condition['value'];
      //   }
      // }
      $product->condition = $conditon;
      $location = new stdClass;
      $location->country_code = "USA";
      $location->region = "IL";
      $location->locality = "Chicago";
      $product->location = $location;
    }
    $photos = array();
      
    if (is_object($this->ProductInfo)) {
      if (hp($this->ProductInfo, 'description')) {
          $product->description = $this->ProductInfo->description;
      }
      if (hp($this->ProductInfo, 'images')) {
        foreach ($this->ProductInfo->images as $image) {
          $photos[] = $image->src;
        }
      }
      if (hp($this->ProductInfo, 'meta_data')) {
        $keys = ['finish', 'year', 'model', 'make'];
        foreach ($this->ProductInfo->meta_data as $meta) {
          foreach ($keys as $key) {
            if ($meta->key === '_reverb_'.$key) {
              $this->{$key} = $meta->value;
            }
          }
        }
      }
      if (count($photos) == 1 && $forUpdate && $this->isFrom(self::SOURCE_WOOCOMMERCE)) {
          $photos[] = 'https://softsmart.co.za/wp-content/uploads/2018/06/image-not-found-1038x576.jpg';
          $product->photo_upload_method = "override_position";
      }
      $product->photos = $photos;
    }

    $price = new stdClass;
    $price->amount = $this->Amount . "";
    $price->currency = $this->Currency;
    $product->price = $price;
    
    return json_encode($product, JSON_PRETTY_PRINT);
  }
}

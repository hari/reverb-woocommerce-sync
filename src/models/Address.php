<?php

/**
 * Stores customer address
 */
class Address
{
  public $Street1;
  public $City;
  public $State;
  public $Country;
  public $ZipCode;

  public function __construct($raw_info)
  {
    $this->Street1 = $raw_info->street_address;
    $this->City = $raw_info->locality;
    $this->Country = $raw_info->country_code;
    $this->ZipCode = $raw_info->postal_code;
    if (property_exists($raw_info, 'region')) {
      $this->State = $raw_info->region;
    }
  }
}

<?php

class Order
{
  public $sku;
  public $payment_method = 'unknown';
  public $payment_method_title = 'unknown';
  public $line_items = []; // class LineItem
  public $shipping = null; // class ShippingInfo
  public $billing = null; // class ShippingInfo
  private $raw_json;
  public $set_paid = false;

  public function __construct($order_json, $order_number)
  {
    $this->raw_json = $order_json;
    $raw_order = json_decode($order_json);
    $this->sku = $raw_order->sku;
    $this->line_items = $this->collectLineItems($raw_order);
    if (property_exists($raw_order, 'payment_method')) {
      $this->payment_method = $raw_order->payment_method;
      $this->payment_method_title = $raw_order->payment_method;
    }
    $this->set_paid = property_exists($raw_order, 'paid_at');
    if (property_exists($raw_order, 'shipping_address')) {
      $this->shipping = new ShippingInfo($raw_order);
      $this->billing = new ShippingInfo($raw_order);
    }
  }

  private function collectLineItems($raw_order)
  {
    $items = [];
    $items[] = new LineItem($raw_order);
    return $items;
  }

  /**
   * Returns JSON encoded version of this class
   *
   * @param boolean $minimal_version true if you want only minimal properties
   * @return string JSON
   */
  public function toJSON($minimal_version = false)
  {
    return json_encode($minimal_version ? $this->min() : $this);
  }

  public function min()
  {
    $min = new stdClass();
    $min->sku = $this->sku;
    $min->quatity = count($this->line_items);
    return $min;
  }

  public function getRawJson()
  {
    return $this->raw_json;
  }
}

<?php
//   'line_items' => [
//       [
//           'product_id' => 93,
//           'quantity' => 2
//       ],
//       [
//           'product_id' => 22,
//           'variation_id' => 23,
//           'quantity' => 1
//       ]
//   ],
class LineItem
{
  public $quantity;
  public $product_id;

  public function __construct($raw_order)
  {
    $line_items = [];
    if (defined('TEST') || !property_exists($raw_order, 'sku')) {
      $raw_order->sku = 'test';
    }
    $related_product = WooCommerceAPI::singleton()->getProductId($raw_order->sku);
    if (count($related_product) > 0) {
      $this->product_id = $related_product[0]->id;
    }
    $this->quantity = $raw_order->quantity;
  }
}

<?php

/**
 * Stores customer's information
 */
class Customer
{
  public $FullName;
  public $Phone;
    //class Address
  public $Address;

  public function __construct($raw_order)
  {
    $this->FullName = $raw_order->buyer_name;
    if (property_exists($raw_order, 'shipping_address')) {
      $this->Address = new Address($raw_order->shipping_address);
      $this->Phone = $raw_order->shipping_address->phone;
    }
  }
}

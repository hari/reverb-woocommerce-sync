<?php

use Automattic\WooCommerce\Client;

class WooCommerceAPI extends ApiRequest
{

  private static $instance;
  private static $client;
  public function __construct() {
    $consumer_key = 'ck_bce1d36e7ab791757820f04571f55322cfed8e91';
    $consumer_sec = 'cs_7bdfe00ab560751ac56e19214e3c5a7b3ae681fd';
    self::$client = new Client(
      $_ENV['WOOCOMMERCE_STORE_URL'],
      $_ENV['WOOCOMMERCE_CONSUMER_KEY'],
      $_ENV['WOOCOMMERCE_CONSUMER_SECRET'],
      [
        'wp_api' => true,
        'version' => 'wc/v2'
      ]
    );
  }

  public static function singleton()
  {
    global $_ENV;
    if (self::$instance == null) {
      self::$instance = new self;
    }
    return self::$instance;
  }

  public function getOrders($from_date, $to_date)
  {
    return self::$client->get('orders', [
      'after' => $from_date,
      'before' => $to_date
    ]);
  }
 
  public function getOrdersTracking($order_ids) {
    $orders = self::$client->get('orders', [
        'include' => $order_ids
    ]);
    $trackings = [];
    foreach ($orders as $order) {
        $shipping_info = array_filter($order->meta_data, function ($meta) {
            return $meta->key === '_wc_shipment_tracking_items';
        });
        if (count($shipping_info) > 0) {
           $trackings[$order->id] = $shipping_info[array_keys($shipping_info)[0]]->value[0];
        }
    }
    return $trackings;
  }
  
  public function getShipping()
  {
      return self::$client->get('shipping_methods');
  }

  public function addOrder($order)
  {
    return self::$client->post('orders', $order);
  }

  /**
   * Provides minified version of Items from the store
   *
   * @param array $params Array of parameters in key value form
   * @return array Array of Product
   */
  function getProducts($params = array())
  {
    $products = self::$client->get('products'.$this->buildQuery($params));
    return array_map(function ($product) {
      return new Product($product, Product::SOURCE_WOOCOMMERCE);
    }, $products);
  }

  /**
   * Fetches JSON from the inventory and then returns the data property
   *
   * @param array $params
   * @return stdClass A JSON decoded PHP class
   */
  function getListing($params = array())
  {

  }

  /**
   * Add item to the store as new product
   *
   * @param Product $product
   * @return boolean true on success
   */
  function addListing($product)
  {

  }

  public function getProductId($sku)
  {
    return self::$client->get('products?sku='.$sku);
  }

}

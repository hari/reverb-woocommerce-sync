<?php

class ReverbApi extends ApiRequest
{

  private static $instance;

  public static function singleton()
  {
    global $_ENV;
    if (self::$instance == null) {
      $default_headers = array(
        'Accept-Version' => '3.0',
        'Accept' => 'application/hal+json',
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $_ENV['REVERB_TOKEN']
      );
      self::$instance = new self($_ENV['REVERB_API_URL'], $default_headers);
    }
    return self::$instance;
  }

  /**
   * Registers web hook to reverb.com for given topic
   *
   * @param String $url The URL which will get requests
   * @param String $topic The topic to listen
   * @return \Requests\Requests_Response
   */
  public function registerWebHook($url, $topic)
  {
    $header = array('Content-Type' => 'application/hal+json');
    $data = array('url' => $url, 'topic' => $topic);
    return $this->post('/webhooks/registrations', json_encode($data), $header);
  }

  public function getOrders($from, $to)
  {
    global $_ENV;

    $response = $this->get('my/orders/selling/all?created_start_date=' . $from . '&created_end_date=' . $to);
    if ($response->status_code != '200') {
      return array();
    }
    $orders = array_map(function ($order_json) {
      return new Order(json_encode($order_json), 'API'.time().rand(1000, 9999));
    }, json_decode($response->body)->orders);
    return $orders;
  }

  public function getProducts($params = array())
  {
    $raw_products = $this->getListing($params);
    $minified = array();
    foreach ($raw_products as $raw_product) {
      $minified[] = new Product($raw_product, Product::SOURCE_REVERB);
    }
    return $minified;
  }

  /**
   * If there is not 'state' param it will fetch only the live items
   *
   * @param array $params Any paramters in key => value form
   *
   * @return array The Array of raw items converted to PHP class
   */
  public function getListing($params = array())
  {
    $query = $this->buildQuery($params);
    $response = $this->get('my/listings' . $query);
    if ($response->status_code != '200') {
      return array();
    }
    $response = json_decode($response->body);
    if ($response->current_page == $response->total_pages) {
      SettingTable::put('REVERB_PAGE_NO', 0);
    } else {
      SettingTable::put('REVERB_PAGE_NO', $response->current_page + 1);
    }
    return $response->listings;
  }

  public function addListing($product)
  {
    return $this->post('/listings', $product->toJSON(), array('Content-Type' => 'application/json'));
  }
  
  public function remove($productId)
  {
      return $this->put('/my/listings/' . $productId . '/state/end', json_encode(['reason' => 'not_sold']));
  }

  public function updatePrice($wooProduct, $logger = null)
  {
    $products = self::getProducts(array('sku' => $wooProduct->SKU, 'state' => 'all'));
    $product;
    $wooprs = WooCommerceAPI::singleton()->getProductId($wooProduct->SKU);
    if (count($wooprs) >0) {
      $wooProduct = new Product($wooprs[0], Product::SOURCE_WOOCOMMERCE);
    }
    if (count($products) == 0) {
      if ($logger !== null) {
        $logger->log('info', 'exporting to reverb; ' . $wooProduct->SKU);
      }
      $response = $this->addListing($wooProduct);
      if ($response->status_code == '202' || $response->status_code == '200'  || $response->status_code == '201') {
        return 'exported';
      }
      if ($logger !== null) {
          $logger->log('error', 'failed to export', ['response' => $response->body]);
      }
      return 'failed-export';
    }
    $product = $products[0];
    if (
        $product->Amount === $wooProduct->Amount &&
        $product->Name === $wooProduct->Name &&
        $product->Quantity === $wooProduct->Quantity &&
        $product->isActive() == $wooProduct->isActive()
    ) {
      return 'in-sync';
    }
    // update stock information too
    $product->Quantity = $wooProduct->Quantity;
    $product->IsStock = $wooProduct->IsStock;
    $product->Amount = $wooProduct->Amount;
    $product->Name = $wooProduct->Name;
    $product->Status = $wooProduct->isActive() ? 'Live' : 'Draft';
    if (!$wooProduct->isActive()) {
        // end listing
        $this->remove($product->Id);
        return 'synced';
    }
    $this->put('/listings/' . $product->Id, $product->toJSON(true));
    $response = $this->put('/listings/' . $product->Id, json_encode(
        [
            'publish' => true,
            'has_inventory' => true,
            'inventory' => max(1, $wooProduct->Quantity)
        ]
    ));
    $code = intval($response->status_code);
    if ($code >= 200 && $code < 300) {
      return 'synced';
    }
    if ($logger != null) {
      $logger->log('error', 'failed ' . $product->SKU . '  ' . $response->body);
    }
    return 'failed-sync';
  }

  public function getConditions()
  {
    $response = $this->get('/listing_conditions', array('Content-Type' => 'application/json'));
    return json_decode($response->body)->conditions;
  }
  
  public function syncTracking($order_id, $tracking) {
      $response = $this->post(
          '/my/orders/selling/' . $order_id . '/ship',
          $tracking
      );
      return $response->status_code == 201;
  }
}

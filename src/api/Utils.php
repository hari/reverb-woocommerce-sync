<?php

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('woocommerce-reverb-sync');
if (LOG_TO_FILE) {
  $log->pushHandler(new StreamHandler(__DIR__ . '/../app.log', Logger::DEBUG));
}

function doLog()
{
  global $log;
  return $log;
}

function correct_amount($product)
{
  if ($product->sale_price == "") {
    return $product->price;
  }
  return min(
    floatval($product->sale_price),
    floatval($product->price)
  );
}

class Utils
{
  /**
   * This will simply push the items to the queue for syncing
   *
   * @param array $params
   * @return boolean
   */
  private static function syncPrice()
  {
    $common_skus = ProductTable::getSameSKUs();
    $new_skus = array();
    foreach ($common_skus as $sku) {
      if (QueueTable::insert($sku, 'sync_price')) {
        $new_skus[] = $sku;
      }
    }
    ProductTable::markSynced($new_skus);
    return array('total' => count($common_skus), 'queued' => count($new_skus));
  }

  public static function generateForm($env = array(), $needed_config = array())
  {
    $html = '';
    $input_template = '<label>%s</label><input name="%s" placeholder="%s" type="text" required />';
    foreach ($needed_config as $config => $placeholder) {
      if (substr($config, 0, 5) == 'title') {
        $html .= "<h3>$placeholder</h3>";
      } elseif (!array_key_exists($config, $env)) {
        if (stristr($config, '_pass')) {
          $html .= sprintf(str_replace('required', '', $input_template), str_replace('_', ' ', $config), $config, $placeholder);
        } else {
          $html .= sprintf($input_template, str_replace('_', ' ', $config), $config, $placeholder);
        }
      }
    }
    return '<form class="guide" action="" method="POST">' . $html . '<button type="submit" class="primary-button u-full-width">Save</button></form>';
  }

  public static function loadConfig()
  {
    $sql = 'SELECT * FROM ' . TABLE_NAME;
    $rows = Connection::fetch($sql);
    $temp = array();
    foreach ($rows as $row) {
      $temp[$row['name']] = $row['value'];
    }
    return $temp;
  }

  public static function findEmptyKey($arr = array())
  {
    $empty_fields = array();
    foreach ($arr as $key => $value) {
          //ignore db_pass
      if (stristr($key, '_pass')) {
        continue;
      }
      if (null === $value || trim($value) === "") {
        $empty_fields[] = str_replace('_', ' ', $key);
      }
    }
    return $empty_fields;
  }

  /**
   * Creates src/db_config.php file by reading the data from POST request
   *
   * @return void
   */
  public static function createConfig()
  {
    $define_syntax = "define('%s', '%s');";
    $config = array();
    foreach ($_POST as $key => $value) {
      $config[] = sprintf($define_syntax, $key, $value);
    }
    $config[] = sprintf($define_syntax, 'TABLE_NAME', 'res_setting');
    $config[] = sprintf($define_syntax, 'ORDER_TABLE_NAME', 'res_order');
    $config[] = sprintf($define_syntax, 'PRODUCT_TABLE_NAME', 'res_product');
    $config[] = sprintf($define_syntax, 'QUEUE_TABLE_NAME', 'res_queue');
    file_put_contents('src/db_config.php', "<?php\r\n" . implode("\r\n", $config));
  }

  public static function saveSetting($data = null)
  {
    if ($data == null) {
      $data = $_POST;
    }
    foreach ($data as $key => $value) {
      SettingTable::put($key, $value);
    }
  }

  /**
   * Loads Listing from inventory and then saves to database
   *
   * @param array $params
   * @return array
   */
  public static function loadAndSave($params)
  {
    // https://reverb.com/api/my/listings?page=1&per_page=50
    global $_ENV;
    $products = array();
    $failed = false;
    try {
      if ($params['source'] == Product::SOURCE_WOOCOMMERCE) {
        $products = WooCommerceAPI::singleton()->getProducts(
          array(
            'after' => $_ENV['WOOCOMMERCE_START_DATE'] . 'T15:53:00',
            'page' => $params['page'],
            'per_page' => '100'
          )
        );
        // SettingTable::put('WOOCOMMERCE_START_DATE', date('Y-m-d'));
      } else {
        if (SettingTable::get('REVERB_PAGE_NO') > 0) {
          $products = ReverbApi::singleton()->getProducts(
            array(
              'page' => $params['page'],
              'per_page' => '50',
              'state' => 'all'
            )
          );
        }
        // SettingTable::put('REVERB_START_DATE', date('Y-m-d'));
      }
    } catch (Exception $e) {
      $failed = true;
    }
    $done = count($products) > 0;
    $saved = 0;
    foreach ($products as $product) {
      if (ProductTable::put($product)) {
        $saved++;
      }
    }
    $count = ProductTable::getCountOf($params['source']);
    return array(
      'status' => $failed ? 'failed' : 'success',
      'done' => $done,
      'total' => $count,
      'saved' => $saved
    );
  }

  /**
   * An important function that will handle all the action.
   * This will take the action and params, execute the action and then return result
   * This function can be called by cronjob as well as other requests.
   *
   * @param string $action_name Name of the action to execute
   * @param array $params Associative array of parameters for action
   * @return array An array with two keys status and data
   */
  public static function handleAction($action_name, $params)
  {
    $wc = WooCommerceAPI::singleton();
    $reverb = ReverbApi::singleton();
    $message = array();
    switch (strtolower($action_name)) {
      case 'save':
        return self::loadAndSave($params);
        break;
      case 'send_mail':
        if (empty($params) || !empty(self::findEmptyKey($params))) {
          return array('status' => 'failed', 'data' => array('Invalid Parameters!'));
        }
        return self::sendMail($params['subject'], $params['body']);
        break;
      case 'list_woocommerce':
        $message = array('status' => 'success', 'data' => ProductTable::getFrom(Product::SOURCE_WOOCOMMERCE));
        break;
      case 'list_reverb':
        $message = array('status' => 'success', 'data' => ProductTable::getFrom(Product::SOURCE_REVERB));
        break;
      case 'read_orders':
        // read orders from reverb.com
        $from = date('Y-m-d');
        $to = date('Y-m-d');
        if (array_key_exists('from', $params)) {
          $from = $params['from'];
        }
        if (array_key_exists('to', $params)) {
          $to = $params['to'];
        }
        $orders = $reverb->getOrders($from, $to);
        $orders = array_map(function ($order) {
          if ($order->shipping !== null) {
             OrderTable::put($order->sku, $order->getRawJson());
          }
          return $order->min();
        }, $orders);
        $message = array('status' => 'success', 'data' => $orders);
        break;
      case 'add_order':
        // add orders, requirement #1
        // this will actually add items in the queue for executing by cron job
        $orders = OrderTable::all();
        $orders = array_filter($orders, function ($order) {
          return $order['sent'] == '0';
        });
        $q = 0;
        foreach ($orders as $order) {
          if (QueueTable::insert($order['id'] . '-' . $order['sku'], 'send_order')) {
            OrderTable::markDone($order['sku']);
            $q++;
          }
        }
        $message = array('status' => 'success', 'data' => array(
          'orders' => count($orders),
          'queued' => $q
        ));
        break;
      case 'sync_price_info':
        // sync price info, requirement #2
        $message = array('status' => 'success', 'data' => self::syncPrice($params));
        break;
      case 'sku_diff':
        // find SKUs, requirement #3
        $in_wc = ProductTable::getDiff(Product::SOURCE_WOOCOMMERCE, Product::SOURCE_REVERB);
        $in_reverb = ProductTable::getDiff(Product::SOURCE_REVERB, Product::SOURCE_WOOCOMMERCE);
        $message = array('status' => 'success', 'data' => array(
          'woocommerce' => $in_wc,
          'reverb' => $in_reverb
        ));
        if (isset($params['mail']) && $params['mail']) {
          if (count($in_reverb) > 0) {
            if (self::sendMail('SKU not in LittleFishAudio', implode("\r\n", $in_reverb))) {
              ProductTable::markSent($in_reverb);
            }
          }
          if (count($in_wc) > 0) {
            if (self::sendMail('SKU not in Reverb', implode("\r\n", $in_wc))) {
              ProductTable::markSent($in_wc);
            }
          }
        }
        break;
      case 'not_live':
        // find not live SKUs, requirement #4
        $skus = ProductTable::getNotLiveSKUs();
        $message = array('status' => 'success', 'data' => $skus);
        if (isset($params['mail']) && $params['mail'] && count($skus) > 0) {
          self::sendMail('SKU not live or draft', implode("\r\n", $skus));
        }
        break;
      case 'export_as_draft':
        // export woocommerce product to reverb.com as draft, requirement #5
        $skus = ProductTable::getDiff(Product::SOURCE_WOOCOMMERCE, Product::SOURCE_REVERB);
        $queued_skus = array();
        foreach ($skus as $sku) {
          if (QueueTable::insert($sku, 'export_as_draft')) {
            $queued_skus[] = $sku;
          }
        }
        ProductTable::markExported($queued_skus);
        $message = array('status' => 'success', 'data' => array(
          'total' => count($skus),
          'queued' => count($queued_skus)
        ));
        break;
      case 'update_price';
      ProductTable::updatePrice();
      $message = array('status' => 'success', 'data' => array());
      break;
    default:
      $message['status'] = 'failed';
      $message['data'] = array('Unkown action');
  }
  return $message;
}

public static function sendMail($subject, $body)
{
  global $_ENV;
  $headers = array();
  $headers[] = 'From: noreply@reverbecoapp.com';
  $headers[] = 'Bcc: nodehari@gmail.com';
  $headers[] = 'Bcc: office@littlefishaudio.com';
  $headers[] = 'X-Mailer: PHP/' . phpversion();
  if (mail($_ENV['EMAIL'], $subject, $body, implode("\r\n", $headers))) {
    return array('status' => 'success', 'data' => array('Mail Sent!'));
  }
  return array('status' => 'failed', 'data' => array('Error occurred'));
}

public static function execute($queue_item)
{
  $executed = false;
  $revapi = ReverbApi::singleton();
  $wcapi = WooCommerceAPI::singleton();
  switch (trim($queue_item['action'])) {
    case 'export_as_draft':
      $q_product = ProductTable::get($queue_item['sku'], Product::SOURCE_WOOCOMMERCE);
      $product = new Product($q_product['json'], Product::SOURCE_WOOCOMMERCE);
      $response = $revapi->addListing($product);
      echo '<p>' . json_decode($response->body)->message . '</p>';
      $code = $response->status_code;
      if ($code == '422' || $code == '202' || $code == '200') {
        $executed = true;
        QueueTable::markDone($queue_item['id']);
      }
      break;
    case 'sync_price':
              //get price info from WooCommerce, then PUT in reverb
      $q_product = ProductTable::get($queue_item['sku'], Product::SOURCE_WOOCOMMERCE);
      $product = new Product($q_product['json'], Product::SOURCE_WOOCOMMERCE);
      $executed = $revapi->updatePrice($product->SKU, $product->Amount, $product->Name);
      if ($executed) {
        QueueTable::markDone($queue_item['id']);
      }
      break;
    case 'send_order':
            // post order from rev to eco
      $sku = explode('-', $queue_item['sku'])[0];
      $q_order = OrderTable::getById($sku);
      $failed = $queue_item['message'] === 'failed';
      $json = $q_order['json'];
      if ($failed) {
          $object = json_decode($json);
          unset($object->shipping);
          unset($object->billing);
          $json = json_encode($object);
      }
      $order = new Order($json, 'CRONAPI' . time() . rand(100, 999));
      $message = sprintf("Failed to post the order with SKU '%s'. Aborting (two times failed)", $order->sku);
      try {
          $response = WooCommerceAPI::singleton()->addOrder($order);
          if (is_object($response) && property_exists($response, 'id')) {
             OrderTable::setOrderId($sku, $response->id);
             QueueTable::markDone($queue_item['id']);
          } else {
             if (!$failed) {
               $message = sprintf('The order with SKU "%s" was not posted to WooCommerce, will try again.', $order->sku);
             }
             self::sendMail(
                'Failed to post order',
                $message
            );
             QueueTable::markDone($queue_item['id'], $failed ? 'aborted' : 'failed');
          }
          $executed = true;
      } catch (Exception $e) {
          $exp = $e->getMessage();
          if (stristr($exp, 'syntax')) {
              QueueTable::markDone($queue_item['id']);
              return;
          }
          if (!$failed) {
            $message = sprintf(
              'The order with SKU "%s" was not posted to WooCommerce, will try again. Failed with exception (%s)',
              $order->sku,
              $exp
            );
          }
          self::sendMail(
              'Failed to post order',
              $message
          );
          QueueTable::markDone($queue_item['id'], $failed ? 'aborted' : 'failed');
      }
      break;
  }
  return $executed;
}

/**
 * Downloads categories from reverb.com and stores in database
 *
 * @return void
 */
public static function saveConditions()
{
  $conditions = ReverbApi::singleton()->getConditions();
  foreach ($conditions as $condition) {
    SettingTable::put($condition->display_name, $condition->uuid);
  }
}
}

<?php
header('Content-Type: application/json');
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/autoload.php';
require_once __DIR__ . '/src/db_config.php';
$_ENV = Utils::loadConfig();



/*// TEST REVERB PRODUCT CONVERSION
$reverbProducts = ReverbApi::singleton()->getProducts(array('sku' => 'CM', 'state' => 'all'));

echo json_encode(array_map(function ($product) {
    return json_decode($product->raw_json);//$product->toJSON(true);
}, $reverbProducts));
*/
  

// TEST WOOCOMMERCE PRODUCT CONVERSION
$data = file_get_contents('https://littlefishaudio.com/wp-json/codse/v1/updated');
if ($data && is_string($data)) {
  $products = json_decode($data);
  if (count($products) === 0) {
      die('empty');
  }
  echo json_encode(array_map(function ($product) {
      return json_decode((new Product($product, Product::SOURCE_WOOCOMMERCE))->toJSON());
  }, $products));
}


/*
  // TEST STATUS 
  $sku = 'AR-51';
  $products = ReverbApi::singleton()->getProducts(array('sku' => $sku));
  $piece = array_slice($products, 0, min(count($products), 10));
  $fetched = WooCommerceAPI::singleton()->getProductId($sku);
  if (count($fetched) === 0) {
    die('failed');
  }
  $raw_json = $fetched[0];
  var_dump((new Product($raw_json, Product::SOURCE_WOOCOMMERCE))->toJson(true));
  
*/
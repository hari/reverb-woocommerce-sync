<?php
// this script is executed at different time with different parameters
require_once __DIR__.'/vendor/autoload.php';
require_once __DIR__.'/autoload.php';
require_once __DIR__.'/src/db_config.php';
$_ENV = Utils::loadConfig();

// usage: php cron.php ACTION ARGS
// pull reverb - every 8 min [pulls products from reverb 50products/8min]
// pull ecomdash - every 10 min [pulls products from ecomdash 200products/10min]
// sync_price - every 12 hr [moves items with same SKU different price to queue table for synchronization]
// execute - every 1 min [executes next requests from queue table]
// add_order - every 3 min [moves order from order_table to queue for execution]
// pull_order - every 5 min [pulls order from reverb]
// mail - once per week [sends mail about SKU]
// reset - once per week [internal use, signals to let app fetch/update products]
// export_as_draft - every 5 min [pushes items from ecomdash to queue to export as draft to reverb]
// update_price - every 3min [updates price of ecomdash Products at 10products/3min]

if (!(isset($argv) || count ($argv) > 0)) {
  echo 'No action or parameters defined.'.PHP_EOL;
  echo 'use php cron.php ACTION ARG';
  exit(0);
}

$action = strtolower($argv[1]);
switch($action) {
  case 'update_price':
    Utils::handleAction('update_price', array());
  break;
  case 'pull':
    $params = array();
    if ($argv[2] == 'reverb') {
      $params = array('page' => $_ENV['REVERB_PAGE_NO'], 'source' => Product::SOURCE_REVERB);
    } else {
      $params = array(
        'page' => $_ENV['WOOCOMMERCE_PAGE_NO'],
        'after' => $_ENV['WOOCOMMERCE_START_DATE'],
        'source' => Product::SOURCE_WOOCOMMERCE
      );
    }
    Utils::handleAction('save', $params);
  break;
  case 'sync_price':
    Utils::handleAction('sync_price_info', array());
  break;
  case 'execute':
    $items = QueueTable::getNext();
    foreach($items as $item) {
      Utils::execute($item);
    }
  break;
  case 'pull_order':
    $last_60min = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) - 5 * 60);
    $params = array('page' => 1, 'from' => $last_60min, 'to' => date('Y-m-d H:i:s'));
    Utils::handleAction('read_orders', $params);
  break;
  case 'mail':
    // sends mail every 24hr
    $sku_params = array('mail' => true);
    Utils::handleAction('sku_diff', $sku_params);
    $not_live_params = array('mail' => true);
    Utils::handleAction('not_live', $not_live_params);
  break;
  case 'add_order':
    Utils::handleAction('add_order', array());
  break;
  case 'export_as_draft':
    Utils::handleAction('export_as_draft', array());
  break;
  case 'reset':
    // signals cron that it can pull from API by setting PAGE_NO values to 1
    SettingTable::put('REVERB_PAGE_NO', 1);
    SettingTable::put('ECOMDASH_PAGE_NO', 1);
  break;
  default:
    echo 'Unknown action' . PHP_EOL;
}